CC=gcc
INCLUDE_FILES=utils.h sha1.h bencode.h
CFLAGS=-g -Wall -O0 -Werror -Wshadow -Wpointer-arith -Wwrite-strings -Wunused
all: pa4
pa4: pa3.o bencode.o utils.o sha1.o bitset.o url_parser.o
	$(CC) $(CFLAGS) $^ -o $@
	chmod a+x pa4
pa1: pa1-listips.o bencode.o utils.o sha1.o
	$(CC) $(CFLAGS) $^ -o $@
clean: 
	rm -f *.o pa1 pa3 pa4
pa1e: pa3.o bencode.o utils.o sha1.o
	$(CC) $(CFLAGS) $^ -o $@ -lefence
pa1m: pa3.o bencode.o utils.o sha1.o
	$(CC) $(CFLAGS) $^ -o $@ -lmallocdebug
submit: .submit submit.jar clean
	java -jar submit.jar edu.umd.cs.submit.CommandLineSubmit
submit.jar:
	wget http://www.cs.umd.edu/class/fall2010/cmsc417/submit.jar

url_parser.o: url_parser.c
	$(CC) -g -c url_parser.c
	
pa3.o: pa3.c
	$(CC) -g -c pa3.c
depend:
	makdepend *.c
