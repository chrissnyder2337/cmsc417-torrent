#include "bitset.h"

#define UNDEFINED 0
#define STATE_UNCONNECTED 1
#define STATE_PEER_INVALID 2
#define SENT_HANDSHAKE_WAITING_FOR_REPLY 3
#define ACCEPTED_WATING_FOR_HANDSHAKE 4
#define STATE_READ_MESSAGES 5
#define CONNECTION_CLOSED 6


#define MESSAGE_CHOKE 0
#define MESSAGE_UNCHOKE 1
#define MESSAGE_INTERESTED 2
#define MESSAGE_NOT_INTERESTED 3
#define MESSAGE_HAVE 4
#define MESSAGE_BITFIELD 5

// defined here: https://wiki.theory.org/BitTorrentSpecification#Peer_wire_protocol_.28TCP.29


struct peer_connection
{
	// STATE OF PEER
	bool set; // weather or this peer connection is real
	unsigned int state;

	//IP ADDRESS
	unsigned int ip[4];
	char hostname[16];

	//PORT
	unsigned short int port;
	char port_str[7];

	//SOCKET
	int sock_fd;

	//BITSET
	bit_set bit_set;

	//INFO HASH
	char info_hash[20];

	//PEER ID
	char peer_id[20];

	// READING BUFFER
	int total_bytes_read; // number of bytes read
	int bytes_parsed; // number of bytes parsed
	int read_buffer_size; // how large our read buffer is
	char * read_buffer; // read buffer


	//Choking status
	int am_choking;
	int am_interested;
	int peer_choking;
	int peer_interested;

	//Requested
	int requested_piece_num;
	int requested_block_num;

};