/** Chris Snyder
  * chris@chrissnyder.org
  *
  * Resources Used:
  * man getaddrinfo
  * http://beej.us/guide/bgnet
  * http://www.lemoda.net/c/read-whole-file/index.html
  * http://draft.scyphus.co.jp/lang/c/url_parser.html
  * http://www.geekhideout.com/urlcode.shtml
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <string.h> /* memset() */
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <string.h>
#include <time.h>
#include "bencode.h"
#include "url_parser.h"
#include "sha1.h"
#include "pa3.h"

#define DESCRIPTIVE false
#define PRETTY false
#define BACKLOG     20  /* Passed to listen() */
#define READ_BUFF_SIZE 512 //how many bytes we are reading at a time from our buffer
#define READ_BUFF_INCREASE_SIZE 512
#define COMMUNICATE_ERROR -1
#define COMMUNICATE_SUCESS 0
#define TIMETOACCEPT_SEC 60

#define SIZE_OF_HANDSHAKE 68

#define MY_PEER_ID "CS417096-%05d-123456" /* My Peer ID which I pass to tracker. */
#define MAX_PEER_CONNECTIONS 100
#define BLOCK_SIZE_BYTES 16384 
// #define BLOCK_SIZE_BYTES 32768 
#define FIND_NUM_BYTES(totalBits) \
  ((totalBits / 8) + ((totalBits % 8 != 0) ? 1 : 0))


#define CEILING_DIV(x,y) (1 + ((x - 1) / y)) //ceiling division
#define FIRST_BLOCK_IN_PIECE(piece_num) \
  (CEILING_DIV(piece_length,BLOCK_SIZE_BYTES) * piece_num)

#define LAST_PIECE_NUM (num_pieces_in_torrent-1)

#define TOTAL_NUMBER_OF_BLOCKS (((num_pieces_in_torrent-1) * CEILING_DIV(piece_length,BLOCK_SIZE_BYTES)) + (CEILING_DIV((file_length - ((num_pieces_in_torrent-1)*piece_length)),BLOCK_SIZE_BYTES)))

#define NUM_BLOCKS_IN_STANDARD_PIECE (CEILING_DIV(piece_length,BLOCK_SIZE_BYTES))

#define NUM_BLOCKS_IN_LAST_PIECE (CEILING_DIV((file_length - ((num_pieces_in_torrent-1)*piece_length)),BLOCK_SIZE_BYTES))

#define IS_LAST_PIECE(piece_num) (piece_num == LAST_PIECE_NUM)

#define NUM_BLOCKS_IN_PIECE(piece_num) ((IS_LAST_PIECE(piece_num))?NUM_BLOCKS_IN_LAST_PIECE:NUM_BLOCKS_IN_STANDARD_PIECE)

#define BLOCK_POS_IN_PIECE(block_num) ((block_num % NUM_BLOCKS_IN_STANDARD_PIECE) * BLOCK_SIZE_BYTES)

#define BLOCK_POS(block_num) (((block_num / NUM_BLOCKS_IN_STANDARD_PIECE) * piece_length) + BLOCK_POS_IN_PIECE(block_num))
//TODO: fix 
#define BLOCK_LENGTH(block_num) \
  ((block_num == (TOTAL_NUMBER_OF_BLOCKS-1))? \
  (file_length - ((piece_length*(num_pieces_in_torrent-1))+((NUM_BLOCKS_IN_LAST_PIECE-1)*BLOCK_SIZE_BYTES))) \
  :(((block_num % NUM_BLOCKS_IN_STANDARD_PIECE) != (NUM_BLOCKS_IN_STANDARD_PIECE - 1))?BLOCK_SIZE_BYTES: (piece_length - ((NUM_BLOCKS_IN_STANDARD_PIECE - 1)*BLOCK_SIZE_BYTES))))

#define PIECE_POS(piece_num) (piece_num * piece_length)

#define BLOCK_NUM_FROM_PIECE_AND_OFFSET(piece_num, offset) \
  ((piece_num * NUM_BLOCKS_IN_STANDARD_PIECE) + (offset/BLOCK_SIZE_BYTES))

#define NUM_BYTES_DOWNLOADED \
  ((Is_Bit_Set(file_bitset,LAST_PIECE_NUM))? \
  (((num_pieces_downloaded-1) * piece_length) + (file_length - PIECE_POS(LAST_PIECE_NUM))) \
  :(num_pieces_downloaded * piece_length))\

#define NUM_BYTES_LEFT (file_length - NUM_BYTES_DOWNLOADED)


  // if(Is_Bit_Set(num_pieces_in_torrent-1)){
  //   total_bytes_downloaded = (num_pieces_downloaded-1) * piece_length + ;
  // }else{
  //   total_bytes_downloaded = num_pieces_downloaded * piece_length;
  // }
/** DEFINE GLOBAL VARIABLES **/
struct peer_connection peer_connections[MAX_PEER_CONNECTIONS]; // Array of peer connections
unsigned int max_peer_key; // number of peers we defined in our struct array

benc_val_t parsed_torrent_file; // Parsed Torrent File
benc_val_t parsed_tracker_responce; // Parsed Torrent File

char * file_filename;
FILE * file_fp;
int file_length;
bit_set file_bitset;
bit_set have_block_bitset;
int piece_length;
int num_pieces_in_torrent;
int num_pieces_downloaded;

char * pieces_hash;

struct parsed_url *parsed_tracker_url; // Parsed url of tracker

unsigned int my_port_int;
char my_port[8];
char my_peer_id[21];
sha1_byte_t torrent_info_hash[SHA1_OUTPUT_SIZE];

unsigned char * url_encoded_torrent_info_hash;
int total_interested_peers;
int total_am_interested_peers;
int total_choked_peers; 
int total_am_choking_peers;
int total_closed_peers;
int total_responsive_peers;
int total_handshake_sent_peers;
int total_awaiting_handshake_peers;
int total_unconnected_peers;
int total_invalid_peers;
int total_read_messages_peers;

int total_bytes_read_by_peers;
int total_bytes_parsed_by_peers;
int total_read_buffer_of_peers;



/**
 * iterate over all peers updating counts
 */
void update_peer_status_counts(){
  total_interested_peers = 0;
  total_am_interested_peers = 0;
  total_choked_peers = 0;
  total_am_choking_peers = 0;
  total_closed_peers = 0;
  total_responsive_peers = 0;
  total_handshake_sent_peers = 0;
  total_awaiting_handshake_peers = 0;
  total_unconnected_peers = 0;
  total_invalid_peers = 0;
  total_read_messages_peers = 0;
  total_bytes_read_by_peers = 0;
  total_bytes_parsed_by_peers = 0;  
  total_read_buffer_of_peers = 0;

  int k;
  for(k=0;k < MAX_PEER_CONNECTIONS; k = k+1){
    if(peer_connections[k].set != 0){
      total_responsive_peers++;

      if(peer_connections[k].peer_interested == 1){
        total_interested_peers++;
      }
      if(peer_connections[k].peer_choking == 1){
        total_choked_peers++;
      }
      if(peer_connections[k].am_choking == 1){
        total_am_choking_peers++;
      }
      if(peer_connections[k].am_interested == 1){
        total_am_interested_peers++;
      }
    }
    if(peer_connections[k].state == STATE_UNCONNECTED){
      total_unconnected_peers++;
    }else if(peer_connections[k].state == STATE_PEER_INVALID){
      total_invalid_peers++;
    }else if(peer_connections[k].state == SENT_HANDSHAKE_WAITING_FOR_REPLY){
      total_handshake_sent_peers++;
    }else if(peer_connections[k].state == ACCEPTED_WATING_FOR_HANDSHAKE){
      total_awaiting_handshake_peers++;
    }else if(peer_connections[k].state == STATE_READ_MESSAGES){
      total_read_messages_peers++;
    }else if(peer_connections[k].state == CONNECTION_CLOSED){
      total_closed_peers++;
    }

    total_bytes_read_by_peers += peer_connections[k].total_bytes_read;
    total_bytes_parsed_by_peers += peer_connections[k].bytes_parsed;  
    total_read_buffer_of_peers += peer_connections[k].read_buffer_size;

  }
}

/**
 * Process has done i out of n rounds,
 *  and we want a bar of width w and resolution r.
 * addapted from http://www.rosshemsley.co.uk/2011/02/creating-a-progress-bar-in-c-or-any-other-console-app/
 */
static inline void loadBar(const char * msg, int x, int n, int r, int w){
    x = x+1;
    // Only update r times.
    if ( x % (n/r) != 0 ) return;
 
    // Calculuate the ratio of complete-to-incomplete.
    float ratio = x/(float)n;
    int   c     = ratio * w;
 
    // Show the percentage complete.
    printf("%s\t%3d%% [", msg, (int)(ratio*100) );
 
    // Show the load bar.
    for (x=0; x<c; x++)
       printf("=");
 
    for (x=c; x<w; x++)
       printf(" ");
 
    // ANSI Control codes to go back to the
    // previous line and clear it.
    printf("]\n\033[F\033[J");
}

/**
 * Process has done i out of n rounds,
 *  and we want a bar of width w and resolution r.
 * addapted from http://www.rosshemsley.co.uk/2011/02/creating-a-progress-bar-in-c-or-any-other-console-app/
 */
static inline void show_stats(){
  update_peer_status_counts();

    printf("\nDownloaded: %d%% (%d of %d pieces)\n",((num_pieces_downloaded*100) / num_pieces_in_torrent),num_pieces_downloaded,num_pieces_in_torrent );
    printf("PEERS\n");
    printf("Responsive: %d\t Unconnectd: %d\tInvalid: %d\tClosed: %d\n",total_responsive_peers,total_unconnected_peers,total_invalid_peers,total_closed_peers);
    printf("Peer Status:\n");
    printf("Choking Me: %d\t Am Choking Peer:%d\n",total_choked_peers,total_am_choking_peers);
    printf("Interested in Me: %d\t Am Interested in Peer:%d\n",total_interested_peers,total_am_interested_peers);
    printf("Handshake Sent: %d\tAwaitng Handshake: %d\tReading State:%d\n",total_handshake_sent_peers,total_awaiting_handshake_peers,total_read_messages_peers);
    printf("READ BUFFERS OF ALL PEERS\n");
    printf("Read:%d\t Parsed: %d\tBuffersize:%d\n",total_bytes_read_by_peers,total_bytes_parsed_by_peers,total_read_buffer_of_peers);
    // ANSI Control codes to go back to the
    // previous line and clear it.
    printf("\033[10F\033[J");
}



/**
 * Checks the validity of the piece we have downloaded
 * piece_num is zero indexed
 */
bool validity_of_piece(int piece_num){
  int piece_pointer;
  piece_pointer = piece_num * piece_length;

  //seek to starting position of piece
  fseek(file_fp,piece_pointer,SEEK_SET);

  //read in the piece into memory
  char piece_buffer[piece_length];
  int num_piece_bytes;
  num_piece_bytes = read(fileno(file_fp), piece_buffer, piece_length);

  //compute the hash of that piece
  sha1_byte_t our_piece_hash[SHA1_OUTPUT_SIZE];
  sha1_state_s sha1_info_val;
  sha1_init(&sha1_info_val);
  sha1_update(&sha1_info_val, (sha1_byte_t *) piece_buffer, num_piece_bytes);
  sha1_finish(&sha1_info_val, our_piece_hash);

  //compare the hash of our piece to that in the torrent file
  if(memcmp(our_piece_hash,&pieces_hash[(piece_num*20)],20) == 0){
    if(DESCRIPTIVE) printf("Have piece %d of %d\n", piece_num, num_pieces_in_torrent);
    return true;
  }

  return false;
}

/**
 * Determine who we have requested a particlular block from, if at all.
 * We return the peer key of the peer who we have requested the block from, -1 otherwise
 */
int block_requested_from(int block_num){
  int i;
  for(i=0;i<MAX_PEER_CONNECTIONS;i=i+1){
    if((peer_connections[i].set != 0) 
      && (peer_connections[i].state != CONNECTION_CLOSED)
      && (peer_connections[i].requested_block_num == block_num)){
        return i;
    }
  }
  return -1;
}


/**
 * Returns true if a block is currently being requested by a peer.
 */
bool has_a_peer_requested_block(int block_num){
  return (block_requested_from(block_num) >= 0);
}


/**
 * finds the next unrequested block in a piece.
 * if all of the pieces have been requested return false.
 */ 
bool unrequested_block_in_piece(int piece_num, int * block_num){
  int i;
  for(i = FIRST_BLOCK_IN_PIECE(piece_num); i <= (FIRST_BLOCK_IN_PIECE(piece_num)+NUM_BLOCKS_IN_PIECE(piece_num)-1); i = i+1 ){
    if((!Is_Bit_Set(have_block_bitset,i)) && (!has_a_peer_requested_block(i))){
      *block_num = i;
      return true;
    }
  }
  return false;
}

/**
 * finds the next unrequested block in a piece.
 * if all of the pieces have been requested return false.
 */ 
bool undownloaded_block_in_piece(int piece_num, int * block_num){
  int i;
  for(i = FIRST_BLOCK_IN_PIECE(piece_num); i <= (FIRST_BLOCK_IN_PIECE(piece_num)+NUM_BLOCKS_IN_PIECE(piece_num)-1); i = i+1 ){
    if(!Is_Bit_Set(have_block_bitset,i)){
      *block_num = i;
      return true;
    }
  }
  return false;
}

/**
 * Gets the next piece block to request from the peer.
 * this functions sets piece_num to the piece the peer should request
 * and sets block_num to the off set of the block we shoulf download.
 * true is returned if we found one we should request, false otherwise
 */
bool next_pieceblock_to_request(int peer_key, int *piece_num, int *block_num){
  int i;
  int found_block_num;
  for(i = 0; i <= (num_pieces_in_torrent-1); i = i+1){
    //check if this is a bit we need and the peer has that bit.
    if(!Is_Bit_Set(file_bitset,i) && Is_Bit_Set(peer_connections[peer_key].bit_set,i)){
      if(unrequested_block_in_piece(i, &found_block_num)){
        *piece_num = i;
        *block_num = found_block_num;
        return true;
      }
    }
  }
  for(i = 0; i <= (num_pieces_in_torrent-1); i = i+1){
    //check if this is a bit we need and the peer has that bit.
    if(!Is_Bit_Set(file_bitset,i) && Is_Bit_Set(peer_connections[peer_key].bit_set,i)){
      if(undownloaded_block_in_piece(i, &found_block_num)){
        *piece_num = i;
        *block_num = found_block_num;
        return true;
      }
    }
  }
  return false;
}

/**
 * Given an index of a peer key, send a handshake message to our peer.
 *
 * Returns -1 on failure, 0 on success
 */
int send_handshake_to_peer(unsigned int peer_key){
  if(DESCRIPTIVE) printf("\t(%d) sending handshake to peer...\n",peer_key);

  const int handshake_message_size = 1+19+8+20+20;
  unsigned char handshake_message[handshake_message_size];
  memset(handshake_message, 0, sizeof(handshake_message)); // set it all to 0
  char BitTorrentprotocol[] = "BitTorrent protocol";
  handshake_message[0] = 19;
  memcpy(handshake_message+1,BitTorrentprotocol,19);
  memcpy(handshake_message+(1+19+8),torrent_info_hash,20);
  memcpy(handshake_message+(1+19+8+20),my_peer_id,20);

  int peer_write_status;
  peer_write_status = write(peer_connections[peer_key].sock_fd,handshake_message,handshake_message_size);
  if(peer_write_status < 0){
    fprintf(stderr, "write to standard out error: %s\n", strerror(peer_write_status));
    return -1;
  }
  return 0;
}

/**
 * Given a socket file descriptor return the peerkey
 */
int get_peer_key_from_sockfd(int sock_fd){
  int i;
  for (i = 0; i < MAX_PEER_CONNECTIONS; i=i+1){
    if(peer_connections[i].set == 1){
      if (peer_connections[i].sock_fd == sock_fd){
        return i;
      }
    }
  }
  return -1;
}

/**
 * Free the readbuffers of all of the peer keys.
 */
void free_peer_connections(){
  int i;
  for (i = 0; i < MAX_PEER_CONNECTIONS; i=i+1){
    free(peer_connections[i].read_buffer);
  }
}

void request_a_piece_from_peer(peer_key){

  // Now that we got a 
  int piece_num, block_num;
  if(next_pieceblock_to_request(peer_key,&piece_num,&block_num)){
    //INITIALIZE OUR MESSAGE
    char request_message[17];
    memset(request_message,0,17);

    // fprintf(stderr,"REQUESTING PIECE:%d BLOCK:%d OFFSET:%d\n",piece_num,block_num,BLOCK_POS_IN_PIECE(block_num));
    // fprintf(stderr,"[%d]NUM_BLOCKS_IN_STANDARD_PIECE / this piece: %d/%d\n",block_num,NUM_BLOCKS_IN_STANDARD_PIECE,NUM_BLOCKS_IN_PIECE(piece_num));
    // fprintf(stderr,"[%d]BLOCK_LENGTH(block_num): %d\n",block_num,BLOCK_LENGTH(block_num));
    // fprintf(stderr,"[%d]file_length: %d\n",block_num,file_length);
    // fprintf(stderr,"[%d]piece_length: %d\n",block_num, piece_length);
    // fprintf(stderr,"[%d]total_pieces: %d\n",block_num,num_pieces_in_torrent);
    // fprintf(stderr,"[%d]BLOCK_SIZE_BYTES: %d\n",block_num,BLOCK_SIZE_BYTES);
    // fprintf(stderr,"[%d]NUM_BLOCKS_IN_LAST_PIECE %d\n",block_num,NUM_BLOCKS_IN_LAST_PIECE);
    // fprintf(stderr,"[%d]TOTAL_NUMBER_OF_BLOCKS: %d\n",block_num,TOTAL_NUMBER_OF_BLOCKS);


    //SET MESSAGE LENGTH
    *((unsigned long *) &request_message[0]) = htonl(13);

    //SET MESSAGE ID
    request_message[4] = 6;

    //SET INDEX OF PIECE
    *((unsigned long *) &request_message[5]) = htonl(piece_num);

    //SET OFFSET OF BLOCK
    *((unsigned long *) &request_message[9]) = htonl(BLOCK_POS_IN_PIECE(block_num));

    //SET LENGTH OF BLOCK
    *((unsigned long *) &request_message[13]) = htonl(BLOCK_LENGTH(block_num));

    int bytes_written;
    bytes_written = write(peer_connections[peer_key].sock_fd,request_message,17);

    //printf("Message: %s",request_message);
    if(bytes_written < 0){
      /*something went wrong */
      fprintf(stderr, "write request message error: %s\n", strerror(bytes_written));
    }
    if(bytes_written > 0){
      peer_connections[peer_key].requested_piece_num = piece_num;
      peer_connections[peer_key].requested_block_num = block_num;
    }
  }
}


/**
 * Send interested message to peer when we know they have a piece we may want
 */

void send_interested_to_peer(peer_key){

  // Now that we got a 
  int piece_num, block_num;
  if(next_pieceblock_to_request(peer_key,&piece_num,&block_num)){
    //INITIALIZE OUR MESSAGE
    char request_message[4+1];
    memset(request_message,0,4+1);

    //SET MESSAGE LENGTH
    *((unsigned long *) &request_message[0]) = htonl(1);

    //SET MESSAGE ID
    request_message[4] = 2;

    int bytes_written;
    bytes_written = write(peer_connections[peer_key].sock_fd,request_message,4+1);

    if(bytes_written < 0){
      /*something went wrong */
      fprintf(stderr, "write request message error: %s\n", strerror(bytes_written));
    }

    if(bytes_written > 0){
      peer_connections[peer_key].am_interested = 1;
    }
  }
}
void send_unchoke_to_peer(peer_key){
  //INITIALIZE OUR MESSAGE
  char unchoke_message[4+1];
  memset(unchoke_message,0,(4+1));

  //SET MESSAGE LENGTH
  *((unsigned long *) &unchoke_message[0]) = htonl(1);

  //SET MESSAGE ID
  unchoke_message[4] = 1;

  int bytes_written;
  bytes_written = write(peer_connections[peer_key].sock_fd,unchoke_message,4+1);

  if(bytes_written < 0){
    /*something went wrong */
    fprintf(stderr, "write unchoke message error: %s\n", strerror(bytes_written));
  }

  if(bytes_written > 0){
    peer_connections[peer_key].am_choking = 0;
  }
}

/**
 * Send a have message to a peer
 */
void send_have_to_peer(peer_key,piece_num){
  //INITIALIZE OUR MESSAGE
  char have_message[4+1+4];
  memset(have_message,0,(4+1+4));

  //SET MESSAGE LENGTH
  *((unsigned long *) &have_message[0]) = htonl(5);

  //SET MESSAGE ID
  have_message[4] = 4;

  //SET HAVE PIECE
  *((unsigned long *) &have_message[5]) = htonl(piece_num);


  int bytes_written;
  bytes_written = write(peer_connections[peer_key].sock_fd,have_message,4+1+4);

  if(bytes_written < 0){
    /*something went wrong */
    fprintf(stderr, "write have message error: %s\n", strerror(bytes_written));
  }

 }


/**
 * Send have messages to all peers that may need the piece
 */
void send_have_messages_to_peers(piece_num){
  int i;
  for(i=0;i<MAX_PEER_CONNECTIONS;i=i+1){
    if((peer_connections[i].set != 0) 
      && (peer_connections[i].state != CONNECTION_CLOSED)
      && (!Is_Bit_Set(peer_connections[i].bit_set,piece_num))){
      send_have_to_peer(i,piece_num);
    }
  }
}

/**
 * Return true if peer has a piece we do not, and false otherwise
 */
bool peer_has_piece_we_need(peer_key){
  int i;
  for(i=0;i<num_pieces_in_torrent;i=i+1){
    if(Is_Bit_Set(peer_connections[peer_key].bit_set,i) && !Is_Bit_Set(file_bitset,i)){
      return true;
    }
  }
  return false;
}

/**
 * Send our current bitfield to peer. 
 * This is probably something after we get a handshake from each peer
 */
void send_bitset_to_peer(peer_key){

  //INITIALIZE OUR MESSAGE
  char bitfield_message[(4+1+FIND_NUM_BYTES(num_pieces_in_torrent))];
  memset(bitfield_message,0,(4+1+FIND_NUM_BYTES(num_pieces_in_torrent)));

  //SET MESSAGE LENGTH
  *((unsigned long *) &bitfield_message[0]) = htonl((1+FIND_NUM_BYTES(num_pieces_in_torrent)));

  //SET MESSAGE ID
  bitfield_message[4] = 5;

  //SET OUR BITFIELD
  memcpy(&bitfield_message[5],file_bitset,FIND_NUM_BYTES(num_pieces_in_torrent));

  int bytes_written;
  bytes_written = write(peer_connections[peer_key].sock_fd,bitfield_message,(4+1+FIND_NUM_BYTES(num_pieces_in_torrent)));

  if(bytes_written < 0){
    /*something went wrong */
    fprintf(stderr, "write request message error: %s\n", strerror(bytes_written));
  }
}

void send_piece_to_peer(peer_key,requested_piece_index,requested_piece_offset,requested_piece_length){
  //TODO: Should we send a piece we might not have??

  //INITIALIZE OUR MESSAGE
  char piece_message[(4+1+4+4+requested_piece_length)];
  memset(piece_message,0,(4+1+4+4+requested_piece_length)); 

  //SET MESSAGE LENGTH
  *((unsigned long *) &piece_message[0]) = htonl(9+requested_piece_length);

  //SET MESSAGE ID
  piece_message[4] = 7;

  //SET INDEX OF PIECE
  *((unsigned long *) &piece_message[5]) = htonl(requested_piece_index);

  //SET OFFSET (BEGIN) OF PIECE
  *((unsigned long *) &piece_message[9]) = htonl(requested_piece_offset);

  //COPY PIECE FROM FILE TO MESSAGE
  int piece_pointer;
  piece_pointer = (requested_piece_index * piece_length) + requested_piece_offset;

  //seek to starting position of piece plus the offset
  fseek(file_fp,piece_pointer,SEEK_SET);

  //read in the piece into message
  int num_bytes_read;
  num_bytes_read = read(fileno(file_fp), &piece_message[13], requested_piece_length);

  if(num_bytes_read != requested_piece_length){
    fprintf(stderr, "was not able to read the number of bytes we need to send: %s\n", strerror(num_bytes_read));
  }


  int bytes_written;
  bytes_written = write(peer_connections[peer_key].sock_fd,piece_message,(4+1+4+4+requested_piece_length));

  if(bytes_written < 0){
    /*something went wrong */
    fprintf(stderr, "write piece message error: %s\n", strerror(bytes_written));
  }

  
}


/** 
 * Reads the message from peer and stores it in the peer's buffer.
 *  returns -1 if should disconect, 0 otherwise
 */
int read_from_peer(int peer_key){

  int bytes_read = 0; //The number of bytes read durring this read.

  // Let Read what we can.
  if(DESCRIPTIVE) printf("Reading what we can from peer %d\n (sock: %d)\n", peer_key,peer_connections[peer_key].sock_fd);
  bytes_read = read(peer_connections[peer_key].sock_fd, &(peer_connections[peer_key].read_buffer[peer_connections[peer_key].total_bytes_read]),(peer_connections[peer_key].read_buffer_size - peer_connections[peer_key].total_bytes_read));

  // reading didn't work for some reason
  if(bytes_read < 0){
    if(DESCRIPTIVE) printf("Reading from peer %d did not work for some reason.. \n", peer_key);
    //fprintf(stderr, "READ ERROR: %s\n", strerror(errno));
    close(peer_connections[peer_key].sock_fd);
    peer_connections[peer_key].state = CONNECTION_CLOSED;
    return 0;
  }

  /** HANDLE A CLOSED CONNECTION **/
  if(bytes_read == 0){
    if(DESCRIPTIVE){printf("Peer %d closed the connection.\n", peer_key);}
    close(peer_connections[peer_key].sock_fd);
    // FREE Appropriate stuff here??
    peer_connections[peer_key].state = CONNECTION_CLOSED;
    return 0;
  }

  //Increace the number of bytes we read.
  peer_connections[peer_key].total_bytes_read += bytes_read;

  // Enure that we have enough buffer for the next read, and allocate more if needed
  if((peer_connections[peer_key].read_buffer_size - peer_connections[peer_key].total_bytes_read) < READ_BUFF_INCREASE_SIZE){
    peer_connections[peer_key].read_buffer_size += READ_BUFF_INCREASE_SIZE;
    peer_connections[peer_key].read_buffer = realloc(peer_connections[peer_key].read_buffer, peer_connections[peer_key].read_buffer_size);
  }

  //PARSE THE MESSAGE AS WE HAVE IT SO FAR
  if(peer_connections[peer_key].read_buffer[peer_connections[peer_key].bytes_parsed] == 'e'){
    if(DESCRIPTIVE) printf("Exit message read. We need to close\n");
    return -1;
  }

  //printf("READBUFFER:%s (%d)  \n", peer_connections[peer_key].read_buffer, peer_connections[peer_key].total_bytes_read);
  // parse what we have so far
  if(peer_connections[peer_key].state == SENT_HANDSHAKE_WAITING_FOR_REPLY || peer_connections[peer_key].state == ACCEPTED_WATING_FOR_HANDSHAKE){
    if(peer_connections[peer_key].total_bytes_read < SIZE_OF_HANDSHAKE){
      if(DESCRIPTIVE){printf("\t%dExpecting reply handshake... but have not read enough bytes for it to be a handshake.\n", peer_key);}
      return 0;
    }

    if(peer_connections[peer_key].read_buffer[peer_connections[peer_key].bytes_parsed] != 19){
      if(DESCRIPTIVE){printf("\t%dFirst byte of handshake is wrong.. closing connection.\n", peer_key);}
      close(peer_connections[peer_key].sock_fd);
      // FREE Appropriate stuff here??
      peer_connections[peer_key].state = CONNECTION_CLOSED;
      return 0;
    }

    //Store peer_id and peer 
    memcpy(peer_connections[peer_key].info_hash,peer_connections[peer_key].read_buffer+peer_connections[peer_key].bytes_parsed+(1+19+8),20);
    memcpy(peer_connections[peer_key].peer_id,peer_connections[peer_key].read_buffer+peer_connections[peer_key].bytes_parsed+(1+19+8+20),20);

    //We have now parsed the entire handshake, increase bytes parsed to reflect that.
    peer_connections[peer_key].bytes_parsed = SIZE_OF_HANDSHAKE;
    if(peer_connections[peer_key].state == ACCEPTED_WATING_FOR_HANDSHAKE){
      // I got the inital handshake i was waiting for... not lets send my handshake.
      send_handshake_to_peer(peer_key);
    }
    //now that we have hand shooken correctly lets send them our bitfield
    send_bitset_to_peer(peer_key);
    peer_connections[peer_key].state = STATE_READ_MESSAGES;
  }

  //now lets get the other messages we might get
  if(peer_connections[peer_key].state == STATE_READ_MESSAGES){

    while((peer_connections[peer_key].total_bytes_read - peer_connections[peer_key].bytes_parsed) >= 4){ //while we have something to parse
      if(DESCRIPTIVE) printf("\tParsing a message from %d\n",peer_key );

      //GET THE MESSAGE LENGTH
      unsigned long int message_length;
      unsigned long int len_pos1;
      unsigned long int len_pos2;
      unsigned long int len_pos3;
      unsigned long int len_pos4;
      message_length = 0;
      len_pos1 = 0;
      len_pos2 = 0;
      len_pos3 = 0;
      len_pos4 = 0;

      len_pos1 = (unsigned long)(peer_connections[peer_key].read_buffer[peer_connections[peer_key].bytes_parsed] & 0xff);
      len_pos2 = (unsigned long)(peer_connections[peer_key].read_buffer[peer_connections[peer_key].bytes_parsed+1] & 0xff);
      len_pos3 = (unsigned long)(peer_connections[peer_key].read_buffer[peer_connections[peer_key].bytes_parsed+2] & 0xff);
      len_pos4 = (unsigned long)(peer_connections[peer_key].read_buffer[peer_connections[peer_key].bytes_parsed+3] & 0xff);

      //if(DESCRIPTIVE) printf("message len: %lx %lx %lx %lx\n",len_pos1,len_pos2,len_pos3,len_pos4);

      message_length = (len_pos1<<24) + (len_pos2<<16) + (len_pos3<<8) + (len_pos4);

      if(DESCRIPTIVE) printf("\t\tThis messge has a length of: %lu\n", message_length);

      //NOW that we have the legnth we should see if we have the whole message
      if((peer_connections[peer_key].total_bytes_read - peer_connections[peer_key].bytes_parsed) < 4 + message_length){ // 4 is the size of the length field
        if(DESCRIPTIVE) printf("\t\tWe don't have the full message: %d of %lu\n", (peer_connections[peer_key].total_bytes_read - peer_connections[peer_key].bytes_parsed),4+message_length);
        break;
      }

      if(message_length == 0){ // Accont for keep alive message
        if(DESCRIPTIVE) printf("\t\tThis is a keep alive message\n");
      }else{
        int message_id;
        message_id = peer_connections[peer_key].read_buffer[peer_connections[peer_key].bytes_parsed+4];

        if(message_id == 0){ //CHOKE MESSAGE
          if(DESCRIPTIVE) printf("\t\tThis is a CHOKE message\n");
          peer_connections[peer_key].peer_choking = 1;


        }else if(message_id == 1){ //UNCHOKE MESSAGE
          if(DESCRIPTIVE) printf("\t\tThis is a UNCHOKE message\n");
          peer_connections[peer_key].peer_choking = 0;

          request_a_piece_from_peer(peer_key);

        }else if(message_id == 2){ //INTERESTED MESSAGE
          if(DESCRIPTIVE) printf("\t\tThis is a INTERESTED message\n");
          peer_connections[peer_key].peer_interested = 1;

          send_unchoke_to_peer(peer_key);

        }else if(message_id == 3){ //NOTINTERESTED MESSAGE
          if(DESCRIPTIVE) printf("\t\tThis is a NOINTERESTED message\n");
          peer_connections[peer_key].peer_interested = 0;
        }else if(message_id == 4){ //HAVE MESSAGE
          if(DESCRIPTIVE) printf("\t\tThis is a HAVE message\n");
          
          unsigned long int bit_pos;
          bit_pos = (unsigned long) ntohl(*(unsigned long *)&peer_connections[peer_key].read_buffer[peer_connections[peer_key].bytes_parsed+5]);

          if(DESCRIPTIVE) printf("\t\tSetting bitset %lu\n", bit_pos);
          Set_Bit(peer_connections[peer_key].bit_set,bit_pos);

          //if we weren't currently interested in the peer... we are now
          if((peer_connections[peer_key].am_interested == 0) && !Is_Bit_Set(file_bitset,bit_pos)){
            send_interested_to_peer(peer_key);
          }

        }else if(message_id == 5){ //BIFIELD MESSAGE
          if(DESCRIPTIVE) printf("\t\tThis is a BITFIELD message\n");

          int num_bit_set_bytes;
          num_bit_set_bytes = ((num_pieces_in_torrent / 8) + ((num_pieces_in_torrent % 8 != 0) ? 1 : 0));
          memcpy(peer_connections[peer_key].bit_set,&peer_connections[peer_key].read_buffer[peer_connections[peer_key].bytes_parsed+5],num_bit_set_bytes);

          //See if they have a piece we need and we havent told them we are interested yet
          if((peer_connections[peer_key].am_interested == 0) && peer_has_piece_we_need(peer_key)){
            send_interested_to_peer(peer_key);
          }
          
        }else if(message_id == 6){ //REQUEST MESSAGE
          if(DESCRIPTIVE) printf("\t\tThis is a REQUEST message\n");
          
          unsigned long requested_piece_index;
          unsigned long requested_piece_offset;
          unsigned long requested_piece_length;

          requested_piece_index = (unsigned long) ntohl(*(unsigned long *)&peer_connections[peer_key].read_buffer[peer_connections[peer_key].bytes_parsed+5]);
          requested_piece_offset = (unsigned long) ntohl(*(unsigned long *)&peer_connections[peer_key].read_buffer[peer_connections[peer_key].bytes_parsed+9]);
          requested_piece_length = (unsigned long) ntohl(*(unsigned long *)&peer_connections[peer_key].read_buffer[peer_connections[peer_key].bytes_parsed+13]);

          // Send the requested peiece to peer
          send_piece_to_peer(peer_key, requested_piece_index, requested_piece_offset, requested_piece_length);

        }else if(message_id == 7){ //PIECE MESSAGE
          if(DESCRIPTIVE) printf("\t\tThis is a PIECE message\n");

          unsigned long data_size;
          unsigned long piece_index;
          unsigned long piece_offset;
          data_size = message_length - 9;
          piece_index = (unsigned long) ntohl(*(unsigned long *)&peer_connections[peer_key].read_buffer[peer_connections[peer_key].bytes_parsed+5]);
          piece_offset = (unsigned long) ntohl(*(unsigned long *)&peer_connections[peer_key].read_buffer[peer_connections[peer_key].bytes_parsed+9]);


          //write the data we got to the file
          fseek(file_fp,(PIECE_POS(piece_index)+piece_offset),SEEK_SET);
          int bytes_wrote;
          bytes_wrote = write(fileno(file_fp), &peer_connections[peer_key].read_buffer[peer_connections[peer_key].bytes_parsed+13], data_size);
          if(bytes_wrote <0){
            fprintf(stderr, "There was an issue writing piece to file\n");
          }

          //Close and reopen file to ensure the file is on disk before tests check for it.
          fclose(file_fp);
          file_fp = fopen(file_filename,"rb+");



          if(bytes_wrote > 0 ){
            if(data_size >= BLOCK_LENGTH(BLOCK_NUM_FROM_PIECE_AND_OFFSET(piece_index,piece_offset))){
              Set_Bit(have_block_bitset,BLOCK_NUM_FROM_PIECE_AND_OFFSET(piece_index,piece_offset));
            }

            // update our bitset if we can
            if(validity_of_piece(piece_index)){
              if(!Is_Bit_Set(file_bitset,piece_index)){
                num_pieces_downloaded++;
                Set_Bit(file_bitset,piece_index);
                send_have_messages_to_peers(piece_index);
              }
            }else{
              Clear_Bit(file_bitset,piece_index);
            }

          }

          
          peer_connections[peer_key].requested_block_num = -1;
          peer_connections[peer_key].requested_piece_num = -1;

          //As long as we are not being choked we should try to reqest another piece.
          if(peer_connections[peer_key].peer_choking != 1){
            request_a_piece_from_peer(peer_key);
          }

        }else if(message_id == 8){ //CANCEL MESSAGE
          if(DESCRIPTIVE) printf("\t\tThis is a CANCEL message\n");
          
        } else if(message_id == 9){ //PORT MESSAGE
          if(DESCRIPTIVE) printf("\t\tThis is a PORT message\n");
          
        } else{ // UNKNOWN ID!!
          if(DESCRIPTIVE) printf("\t\tThis is an UNKNOWN message. Don't know how to handle message id: %d\n",message_id);
          
        }
      }

      //NOW we need to advance to the next message
      peer_connections[peer_key].bytes_parsed += 4+message_length;
    }
  }
  return 0;
}

/** 
 * Set a file descriptor to blocking or non-blocking mode.
 *
 * @param fd The file descriptor
 * @param blocking 0:non-blocking mode, 1:blocking mode
 *
 * @return 1:success, 0:failure.
 **/
int fd_set_blocking(int fd, int blocking) {
    /* Save the current flags */
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags == -1)
        return 0;
    if (blocking)
        flags &= ~O_NONBLOCK;
    else
        flags |= O_NONBLOCK;
    return fcntl(fd, F_SETFL, flags) != -1;
}

/**
 * Connect to all peers advertised by the tracker
 */
void connect_to_all_peers(){

  if(DESCRIPTIVE) printf("Connecting to all peers...\n");

  unsigned int i;
  for(i=0; i < MAX_PEER_CONNECTIONS; i = i+1){
    if(peer_connections[i].set != 0){
      if(DESCRIPTIVE) printf("\t(%d) %s:%s\n",i,peer_connections[i].hostname,peer_connections[i].port_str);
      struct addrinfo hints; /* Used to define our address structure */
      memset(&hints,0,sizeof(hints)); /* make sure the addr info is empty */

      /* Define the types of addresses we want */
      hints.ai_family = AF_INET; /* We want IPv4 addresses */
      hints.ai_flags = 0; /* not really sure */
      hints.ai_socktype = SOCK_STREAM; /* We want a strea */
      hints.ai_protocol = 0; /* any protocol we can get son */
      
      /* Lets get are address stuff using get addrinfo */
      int status; /* used to hold status of getaddrinfo */

      /* this guy will point to result of getaddrinfo
       * (a linked list of 1 or more struct addrinfos)
       */
      struct addrinfo *serv_result;
      if(DESCRIPTIVE) printf("\t\t(%d) getting address info\n",i);
      /* now we can finally get are address infomation! */
      status = getaddrinfo(peer_connections[i].hostname, peer_connections[i].port_str, &hints, &serv_result);

      /* check if the our getting of address informaton was sugcessful and exit if not*/
      if(status != 0){
        fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
      }

      if(DESCRIPTIVE) printf("\t\t(%d) creating a socket\n",i);
      /* GET THE SOCKET FILE DESCRIPTOR */
      peer_connections[i].sock_fd = socket(serv_result->ai_family, serv_result->ai_socktype, serv_result->ai_protocol);
      
      if(DESCRIPTIVE) printf("\t\t(%d) setting socket to be non blocking\n",i);
      // set the socket to be non-blocking
      fcntl(peer_connections[i].sock_fd, F_SETFL, O_NONBLOCK);

      /* CONNECT! */
      if(DESCRIPTIVE) printf("\t\t(%d) connecting through socket..\n",i);
      int connect_status;
      connect_status = connect(peer_connections[i].sock_fd, serv_result->ai_addr, serv_result->ai_addrlen);

      //freeaddrinfo(serv_result);
      //freeaddrinfo(&hints);

      if(connect_status == 0){
        if(DESCRIPTIVE) printf("\t\t(%d) connected!! \n",i);
        if(send_handshake_to_peer(i)<0){
          //failed to send handshake to peer
          if(DESCRIPTIVE) printf("\t\t(%d) Failed to send handshake to peer, marking as STATE_PEER_INVALID \n",i);
          peer_connections[i].state = STATE_PEER_INVALID;
          peer_connections[i].set = 0;
        }else{
          peer_connections[i].state = SENT_HANDSHAKE_WAITING_FOR_REPLY;
        }
      }else if(connect_status == -1){
        if(errno != EINPROGRESS){ // we are expecting this becasue it is non blocking
          fprintf(stderr, "%s\n", strerror(errno));
        }else{
          if(DESCRIPTIVE) printf("\t\t(%d) connection was not able to be completed immediately\n",i);
          peer_connections[i].state = STATE_UNCONNECTED;
        }
      }
    }
  }
}


/**
 * Connects to host on port, sends message, gets responce.
 *
 * This has been adopted from my p0
 *
 * @param  host     [description]
 * @param  port     [description]
 * @param  request  [description]
 * @param  responce [description]
 * @return          the number of bytes read
 */
int communicate(char hostname[], char port[], char request_buffer[], char **responce_buffer){

  struct addrinfo hints; /* Used to define our address structure */

  memset(&hints,0,sizeof(hints)); /* make sure the addr info is empty */

  /* Define the types of addresses we want */
  hints.ai_family = AF_INET; /* We want IPv4 addresses */
  hints.ai_flags = 0; /* not really sure */
  hints.ai_socktype = SOCK_STREAM; /* We want a stream */
  hints.ai_protocol = 0; /* any protocol we can get son */

  /* Lets get are address stuff using get addrinfo */
  int status; /* used to hold status of getaddrinfo */

  /* this guy will point to result of getaddrinfo
   * (a linked list of 1 or more struct addrinfos)
   */
  struct addrinfo *serv_result;

  /* now we can finally get are address infomation! */
  status = getaddrinfo(hostname, port, &hints, &serv_result);

  /* check if the our getting of address informaton was sugcessful and exit if not*/
  if(status != 0){
    fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
    return COMMUNICATE_ERROR;
  }

  /* GET THE SOCKET FILE DESCRIPTOR */
  int sock_fd;
  sock_fd = socket(serv_result->ai_family, serv_result->ai_socktype, serv_result->ai_protocol);

  /* CONNECT! */
  connect(sock_fd, serv_result->ai_addr, serv_result->ai_addrlen);

  /* write to the socket */
  int bytes_written;
  bytes_written = write(sock_fd,request_buffer,strlen(request_buffer)+1);

  // printf("bytes %i\n", bytes_written);
  if(bytes_written < 0){
    /*something went wrong */
    fprintf(stderr, "write error: %s\n", strerror(bytes_written));
    return COMMUNICATE_ERROR;
  }

  /* shut down the writing part of the socket */
  shutdown(sock_fd, 1); /* 1 maeans further sends are disallowed*/

  /* lets now write on our socket */
  char *read_buffer = malloc(READ_BUFF_SIZE);

  //TODO: we should prolly keep track of number of bytes read.
  int bytes_read = 0;
  int total_bytes_read = 0;
  int read_buffer_size = READ_BUFF_SIZE;

  do{
    bytes_read = read(sock_fd, read_buffer+total_bytes_read,read_buffer_size - total_bytes_read);

    if(bytes_read < 0){
      fprintf(stderr, "read error: %s\n", strerror(bytes_read));
      return COMMUNICATE_ERROR;
    }

    //realocate more space if we need it;

    if(bytes_read > 0){
      total_bytes_read = total_bytes_read + bytes_read;
      read_buffer_size = read_buffer_size * 2;
      read_buffer = realloc(read_buffer, read_buffer_size);
    }
  }while (bytes_read > 0);

  *responce_buffer = read_buffer;
  /* lets close our connection */
  close(sock_fd);

  return total_bytes_read;
}

/* This routine returns the size of the file it is called with.
 * Adapted from http://www.lemoda.net/c/read-whole-file/index.html
 */
static unsigned get_file_size (const char * file_name){
    struct stat sb;
    if (stat (file_name, & sb) != 0) {
        fprintf (stderr, "'stat' failed for '%s': %s.\n",
                 file_name, strerror (errno));
        exit (EXIT_FAILURE);
    }
    return sb.st_size;
}

/**
 * This routine reads the entire file into memory.
 * Adapted from http://www.lemoda.net/c/read-whole-file/index.html
 */
static unsigned char * read_whole_file (const char * file_name){
    unsigned s;
    unsigned char * contents;
    FILE * f;
    size_t bytes_read;
    int status;

    s = get_file_size (file_name);
    contents = malloc (s + 1);
    if (! contents) {
        fprintf (stderr, "Not enough memory.\n");
        exit (EXIT_FAILURE);
    }

    f = fopen (file_name, "r");
    if (! f) {
        fprintf (stderr, "Could not open '%s': %s.\n", file_name,
                 strerror (errno));
        exit (EXIT_FAILURE);
    }
    bytes_read = fread (contents, sizeof (unsigned char), s, f);
    if (bytes_read != s) {
        fprintf (stderr, "Short read of '%s': expected %d bytes "
                 "but got %d: %s.\n", file_name, s, (int)bytes_read,
                 strerror (errno));
        exit (EXIT_FAILURE);
    }
    status = fclose (f);
    if (status != 0) {
        fprintf (stderr, "Error closing '%s': %s.\n", file_name,
                 strerror (errno));
        exit (EXIT_FAILURE);
    }
    return contents;
}

/* Converts an integer value to its hex character*/
char to_hex(char code) {
  static char hex[] = "0123456789abcdef";
  return hex[code & 15];
}

/* Returns a url-encoded version of str */
/* IMPORTANT: be sure to free() the returned string after use */
// unsigned char *url_encode(unsigned char *str,int str_size) {
//   unsigned char *pstr = str;
//   unsigned char *buf = malloc((str_size * 3) +1);
//   unsigned char *pbuf = buf;
//   while (*pstr) {
//     if (isalnum(*pstr) || *pstr == '-' || *pstr == '_' || *pstr == '.' || *pstr == '~')
//       *pbuf++ = *pstr;
//     else if (*pstr == ' ')
//       *pbuf++ = '+';
//     else
//       *pbuf++ = '%', *pbuf++ = to_hex(*pstr >> 4), *pbuf++ = to_hex(*pstr & 15);
//     pstr++;
//   }
//   *pbuf = '\0';
//   return buf;
// }

unsigned char *url_encode(unsigned char *str,int str_size) {
  const char *hex = "0123456789abcdef";
  // unsigned char *pstr = str;
  unsigned char *buf = malloc((str_size * 3) +1);
  unsigned char *pbuf = buf;
  int i;
  for(i=0; i<str_size;i++){
    if( ('a' <= str[i] && str[i] <= 'z')
    || ('A' <= str[i] && str[i] <= 'Z')
    || ('0' <= str[i] && str[i] <= '9') ){
      *pbuf++ = str[i];
    } else {
      *pbuf++ = '%';
      *pbuf++ = (hex[str[i] >> 4]);
      *pbuf++ = (hex[str[i] & 15]);
    }
  }
  *pbuf = '\0';
  return buf;
}





/**
 * Parse the torrent file. The parsed torrent file is stored in the globally defined
 * parsed_torrent_file;
 */
void parse_torrent_file(char * torrent_filename){
 
  /* LOAD ENTIRE TORRENT FILE INTO MEMORY */
  int torrent_file_size;
  torrent_file_size = get_file_size ((const char *)torrent_filename);
  unsigned char * torrent_file_contents;
  torrent_file_contents = read_whole_file(torrent_filename);

  /* PARSE THE TORRENT FILE */
  unsigned char * end;
  tr_bencLoad(torrent_file_contents,torrent_file_size,&parsed_torrent_file,&end);
}

/**
 * Print state of all connected Peers
 * This is the ipaddress port and bitset
 */
void print_peers_state(){
  int k;
  for(k=0;k < MAX_PEER_CONNECTIONS; k = k+1){
    if(peer_connections[k].set != 0){
      printf("> %s:%s ",peer_connections[k].hostname, peer_connections[k].port_str);
      Print_Bit_Set(peer_connections[k].bit_set,num_pieces_in_torrent);
      printf("\n");
    }
  }
}

/** 
  * Parse the tracker url. 
  * This function assumes that parsed_tracker_url is global and the torrent file has already been parsed.
  *
  * The values can be access via:
  * parsed_tracker_url->host
  * parsed_tracker_url->port
  * parsed_tracker_url->path
  * parsed_tracker_url->query
  */
void parse_tracker_url(){

  /* GET THE URL OF THE TRACKER. DEFINED AS ANNOUNCE. */
  benc_val_t * announce_val;
  announce_val =  tr_bencDictFindFirst(&parsed_torrent_file,"announce");
  char * announce_url;
  announce_url = tr_bencStealStr(announce_val);

  /* EXTRACT HOST PORT AND PATH OF TRACKER URL */
  parsed_tracker_url = parse_url(announce_url);
}


void set_piece_length(){
  benc_val_t * info_dict;
  benc_val_t * length_val;

  info_dict = tr_bencDictFind(&parsed_torrent_file,"info");
  length_val =  tr_bencDictFind(info_dict,"piece length");
  
  piece_length = length_val->val.i;
}

void set_number_of_pieces(){
  num_pieces_in_torrent = (file_length % piece_length) ? (file_length / piece_length + 1) : (file_length / piece_length);
}

/** 
 * From the parsed bencoded file get the length of the file.
 * This only works when the file is in Single File Mode.
 */  
void set_length_of_file(){
  benc_val_t * info_dict;
  benc_val_t * length_val;
  unsigned int length_of_file;

  info_dict = tr_bencDictFind(&parsed_torrent_file,"info");
  length_val =  tr_bencDictFind(info_dict,"length");
  
  length_of_file = length_val->val.i;

  file_length = length_of_file;
}

/**
 * From the parsed bencoded torrent file get the filename of the file we are downloading. 
 * This only works when the file is in Single File Mode.
 */
void set_filename_of_file(){
  benc_val_t * info_dict;
  benc_val_t * filename_val;

  info_dict = tr_bencDictFind(&parsed_torrent_file,"info");
  filename_val =  tr_bencDictFind(info_dict,"name");
  
  file_filename = tr_bencStealStr(filename_val);
}

/**
 * From the parsed bencoded torrent file get the pieces. 
 * This only works when the file is in Single File Mode(maybe not tho)
 */
void set_pieces_hash(){
  benc_val_t * info_dict;
  benc_val_t * pieces_val;

  info_dict = tr_bencDictFind(&parsed_torrent_file,"info");
  pieces_val =  tr_bencDictFind(info_dict,"pieces");
  
  pieces_hash = tr_bencStealStr(pieces_val);
}


/** 
 * From the parsed bencoded file get the info hash.
 */  
void set_torrent_info_hash(){

  /* COMPUTE THE INFO HASH. */
  benc_val_t * info_val;
  info_val =  tr_bencDictFindFirst(&parsed_torrent_file,"info");
  
  //get a copy of the info along with its len
  int info_length = 0;
  char * str_info_val;
  str_info_val = tr_bencSaveMalloc(info_val,&info_length);

  /* sha1er the val */
  sha1_state_s sha1_info_val;
  sha1_init(&sha1_info_val);
  sha1_update(&sha1_info_val, (sha1_byte_t *) str_info_val, info_length);
  sha1_finish(&sha1_info_val, torrent_info_hash);

  //hack to truncate 20 bytes... for some reason i was always getting more
  torrent_info_hash[20] = '\0';

  url_encoded_torrent_info_hash = url_encode(torrent_info_hash,20);
}

/**
 *  Compute and return my peer_id
 */
void set_my_peer_id(){
  /* CREATE OUR PEER ID */
  snprintf(my_peer_id, sizeof my_peer_id, "CS417096-%05d-12345",getpid());
}

/**
 * Send stop request to tracker
 */
void send_stop_to_tracker(){

  // unsigned int length_val_val;
  // length_val_val = file_length;

   /* CONNECT TO TRACKER AND SEND THE STOP COMMAND */
  char get_stop_request[256] = "";
  snprintf(get_stop_request, sizeof get_stop_request, "%s%s%s%s%s%s%s%s%s%s%u%s",
    "GET /", parsed_tracker_url->path, "?",
    "info_hash=", url_encoded_torrent_info_hash,
    "&peer_id=", my_peer_id,
    "&port=",my_port,"&compact=1&left=",NUM_BYTES_LEFT,
    "&downloaded=0&uploaded=0&event=stopped HTTP/1.1\r\nHost: scriptroute.cs.umd.edu\r\n\r\n");

  /* CONNECT TO THE TRACKER AND SEND THE GET REQUEST */
  char *stop_responce;
  communicate(parsed_tracker_url->host,parsed_tracker_url->port,get_stop_request, &stop_responce);
}

/** 
 * Announce ourselves to the tracker and get the reponce
 */
void announce_ourselves_to_tracker(){

  //unsigned int length_val_val;
  //length_val_val = file_length;

  /* CONSTRUCT GET REQUEST THAT WE WILL SEND TO THE TRACKER */
  char get_request[256] = "";
  snprintf(get_request, sizeof get_request, "%s%s%s%s%s%s%s%s%s%s%u%s",
    "GET /", parsed_tracker_url->path, "?",
    "info_hash=", url_encoded_torrent_info_hash,
    "&peer_id=", my_peer_id,
    "&port=",my_port,"&compact=1&left=",NUM_BYTES_LEFT,
    "&downloaded=0&uploaded=0&event=started HTTP/1.1\r\nHost: scriptroute.cs.umd.edu\r\n\r\n");

  /* CONNECT TO THE TRACKER AND SEND THE GET REQUEST */
  int resp_size;
  char *responce;
  resp_size = communicate(parsed_tracker_url->host,parsed_tracker_url->port,get_request, &responce);

  if(resp_size < 1){
    fprintf(stderr, "%s\n", "Could not get reponce from tracker");
    exit(EXIT_FAILURE);
  }

  /* READ THE RESPONCE FROM THE TRACKER */
  /* PARSE THE BODY OF THE RESPONCE */
  //move pointer to after headers
  char * responce_body;
  responce_body = strstr(responce, "\r\n\r\n");
  if(responce_body){
    responce_body = responce_body + 4; // move past the \r\n\r\n we found
  }

  int responce_body_size = 0;
  responce_body_size = resp_size - (responce_body - responce);

  unsigned char * responce_end;
  tr_bencLoad(responce_body,responce_body_size,&parsed_tracker_responce,&responce_end);

  /* FIND PEERS IN RESPONCE IF EXISTS. OTHERWISE EXIT */
  benc_val_t * peers_val;
  peers_val =  tr_bencDictFindFirst(&parsed_tracker_responce,"peers");

  /* LOOP THOUGH PEERS AND ADD THEM TO PEER CONNECTIONS */
  unsigned int j;
  for (j=0; j<peers_val->val.s.i; j = j+6){
    unsigned int peer_key = j/6;
    max_peer_key = peer_key;
    unsigned char * one_peer = (unsigned char *)peers_val->val.s.s+j; //only read 6
    unsigned short int one_peer_port;
    one_peer_port = (one_peer[4]<<8)+one_peer[5];

    peer_connections[peer_key].ip[0] = one_peer[0];
    peer_connections[peer_key].ip[1] = one_peer[1];
    peer_connections[peer_key].ip[2] = one_peer[2];
    peer_connections[peer_key].ip[3] = one_peer[3];
    peer_connections[peer_key].port = one_peer_port;
    peer_connections[peer_key].set = 1;

    snprintf(peer_connections[peer_key].hostname, sizeof peer_connections[peer_key].hostname, "%d.%d.%d.%d",one_peer[0],one_peer[1],one_peer[2],one_peer[3]);
    snprintf(peer_connections[peer_key].port_str, sizeof peer_connections[peer_key].port_str, "%d",one_peer_port);
  }
}

/**
 * Initialize peer array
 */
void initialize_peer_connections(){
  // INITIALLIZE OUR peer_connections struct
  unsigned int i;
  for(i=0;i<MAX_PEER_CONNECTIONS;i=i+1){
    peer_connections[i].set = 0;
    peer_connections[i].state = STATE_UNCONNECTED;
    peer_connections[i].ip[0] = 0;
    peer_connections[i].ip[1] = 0;
    peer_connections[i].ip[2] = 0;
    peer_connections[i].ip[3] = 0;
    memset(peer_connections[i].hostname,0,sizeof(peer_connections[i].hostname));
    peer_connections[i].port = 0;
    memset(peer_connections[i].port_str,0,sizeof(peer_connections[i].port_str));
    peer_connections[i].sock_fd = 0;
    peer_connections[i].bit_set = Create_Bit_Set(num_pieces_in_torrent);
    memset(peer_connections[i].info_hash,0,sizeof(peer_connections[i].info_hash));
    memset(peer_connections[i].peer_id,0,sizeof(peer_connections[i].peer_id));
    peer_connections[i].total_bytes_read = 0;
    peer_connections[i].bytes_parsed = 0;
    peer_connections[i].read_buffer_size = READ_BUFF_SIZE;
    peer_connections[i].read_buffer = malloc(READ_BUFF_SIZE);
    peer_connections[i].am_choking = 1;
    peer_connections[i].am_interested = 0;
    peer_connections[i].peer_choking = 1;
    peer_connections[i].peer_interested = 0;
    peer_connections[i].requested_piece_num = -1;
    peer_connections[i].requested_block_num = -1;
  }
}

bool new_file = false;

/**
 * Open file descriptor to the file we are downloading.
 * The file is created if it does not exist.
 */
void open_file(){

  //Open file for reading and writing
  file_fp = fopen(file_filename,"rb+");

  //If the file does not exist we will create it and expand it to it's full size.
  if(file_fp == NULL){
    new_file = true;
    // Create a new file for writing... will begin at beining of file
    file_fp = fopen(file_filename,"w+");
    if(file_fp == NULL){
      fprintf(stderr, "Unable to create a file to write to. \n");
      exit(EXIT_FAILURE);
    }

    ///exand file here to the length we will need
    fseek(file_fp,file_length-1,SEEK_SET);
    fputc('\n',file_fp);

    // "save and re-open"
    fclose(file_fp);
    file_fp = fopen(file_filename,"rb+");
  }
}




/**
 * Using the file opened from disk, compute the bitset.
 * this bitset will represent the peices which we have downloaded.
 */
void compute_file_bitset(){
  int i;
  file_bitset = Create_Bit_Set(num_pieces_in_torrent);

  //If this is a new file... we dont really need to check it
  if(new_file){
    return;
  }

  for(i = 0; i <= (num_pieces_in_torrent-1); i = i +1){
    if (PRETTY) loadBar("Vaidating File",i,(num_pieces_in_torrent-1),100,40);
    if(validity_of_piece(i)){
      if(DESCRIPTIVE) printf("Found VALID Piece: %d\n", i);
      if(!Is_Bit_Set(file_bitset, i)){
        Set_Bit(file_bitset, i);
        num_pieces_downloaded++;
      }
    }else{
      Clear_Bit(file_bitset, i);
    }
  }

  if(DESCRIPTIVE) printf("Bitset of partial file:\n");
  if(DESCRIPTIVE) Print_Bit_Set(file_bitset,num_pieces_in_torrent);
  if(DESCRIPTIVE) printf("\n");
}


/**
 * Initialize the have block bit set
 */ 
void initialize_have_block_bitset(){
  have_block_bitset = Create_Bit_Set(TOTAL_NUMBER_OF_BLOCKS);
}


/**
 * argc is the number of arguments
 * argv[1] will be the filename of the torrent
 */
int main(int argc, char *argv[]){

  struct timeval execution_start_time; //hold the start time
  char * torrent_filename; // torrent filename
  int server_sock; //server socket file descriptor

  // lets get the time of day
  gettimeofday(&execution_start_time,NULL);

  /* THE STUFF WE GET FROM THE USER */
  torrent_filename = argv[1];

  // PARSE A BUNCH OF STUFF FROM THE TORRENT FILEs
  parse_torrent_file(torrent_filename);
  parse_tracker_url();
  set_my_peer_id();
  set_torrent_info_hash(); 
  set_length_of_file();
  set_piece_length();
  set_number_of_pieces();
  set_pieces_hash();

  set_filename_of_file();
  open_file();

  // compute the bitset of the partial files
  compute_file_bitset();


  initialize_have_block_bitset();

  initialize_peer_connections();

  //CREATE THE SERVER SERVER SOCKET
  //lets define the starting number of my port
  fd_set readsocks;
  fd_set writesocks;
  
  if(DESCRIPTIVE) printf("Trying to obtain a port to communicate on");
  /* Create the socket */
  server_sock = socket(AF_INET, SOCK_STREAM, 0);
  if (server_sock == -1) {
    perror("socket");
  }

  struct sockaddr_in server_address;
  server_address.sin_family = AF_INET;
  server_address.sin_addr.s_addr = htonl(INADDR_ANY);
  server_address.sin_port = htons(0); // allows the computer to pick a port for us
  if (bind(server_sock, (struct sockaddr *)&server_address, sizeof(server_address)) < 0) {
    exit(2);
  }

  socklen_t server_addr_len;
  server_addr_len = sizeof(server_address);
  getsockname(server_sock, (struct sockaddr *)&server_address, &server_addr_len);

  my_port_int = (int) htons(server_address.sin_port);
  snprintf(my_port, 8, "%d", my_port_int);

  // set our server socket to be non-blocking
  //fcntl(server_sock, F_SETFL, O_NONBLOCK);

  /* Listen */
  if (listen(server_sock, BACKLOG) == -1) {
    perror("listen");
    return 1;
  }

  //tell the tracker that we want to torrent and populate the peer_connections
  announce_ourselves_to_tracker();

  //create a socket for each peer
  connect_to_all_peers();

  //Keep track of how long we are going to stay alive
  int timelimit_sec = TIMETOACCEPT_SEC;
  struct timeval now_time;
  gettimeofday(&now_time,NULL);

  /* Main loop only run while we still have time */
  while ((now_time.tv_sec - execution_start_time.tv_sec) < timelimit_sec){

    if(PRETTY) show_stats();  
    if(DESCRIPTIVE) printf("TIME LEFT: %d of %d seconds\n",(int)(now_time.tv_sec - execution_start_time.tv_sec),timelimit_sec);
    int s;
    int select_status;
    int maxsock;
    maxsock = 0;

    /* SET UP OUR SET OF FILE DESCRIPTORS */
    FD_ZERO(&readsocks);
    FD_ZERO(&writesocks);

    //add all of our peer's sockets to the fdset
    int k;
    // if(DESCRIPTIVE) printf("Constructing sets of file descriptors: \n------\n");
    for(k=0;k < MAX_PEER_CONNECTIONS; k = k+1){
      if(peer_connections[k].set != 0 && peer_connections[k].state != STATE_PEER_INVALID && peer_connections[k].state != CONNECTION_CLOSED){
        
        // If peer has not been fully connected yet, we want to write
        if(peer_connections[k].state == STATE_UNCONNECTED){
          // if(DESCRIPTIVE) printf("\tSetting socket, %d, for writing (peer ID: %d) \n",peer_connections[k].sock_fd,k);
          FD_SET(peer_connections[k].sock_fd,&writesocks);

        }else{ 
          // All other peers should be set for reading
          // if(DESCRIPTIVE) printf("\tSetting socket, %d, for reading (peer ID: %d) \n",peer_connections[k].sock_fd,k);
          FD_SET(peer_connections[k].sock_fd,&readsocks);
        }

        //increase max sock if we have to
        if(peer_connections[k].sock_fd > maxsock){
          maxsock = peer_connections[k].sock_fd;
        }
      }
    }
    if(DESCRIPTIVE) printf("\n------\n\n");


    //add our server socket file descriptor to 
    if(DESCRIPTIVE) printf("\tSetting server socket, %d, for reading \n",server_sock);
    FD_SET(server_sock, &readsocks);

    //double check that we have the maxsock set to the highest fd
    if(server_sock > maxsock){
      maxsock = peer_connections[k].sock_fd;
    }

    //if(DESCRIPTIVE) printf("\tMaxsock is %d\n",(unsigned int)maxsock);

    //set execution time for only a minute
    struct timeval timeout;
    timeout.tv_sec = timelimit_sec - (now_time.tv_sec - execution_start_time.tv_sec);
    timeout.tv_usec = 0;

    if(DESCRIPTIVE) printf("\tCalling select...\n");
    select_status = select(maxsock + 1, &readsocks, &writesocks, NULL, &timeout); //TODO: deal with eceptionfds
    if (select_status == -1) {
      fprintf(stderr, "Failed on calling select: %s\n", strerror(errno));
      //TODO: figure out what to do if select fails here.
    }

    //If select returns nothing this means we timed out.
    if(select_status == 0){
      if(DESCRIPTIVE) printf("\tSelect timed out. (returned 0)\n");
      break;
    }

    //TODO: FIX MAX SOCK

    // loop thorough our file descriptors and see what we gots
    if(DESCRIPTIVE) printf("\tLooping through all of the possible socket fds... \n");
    for (s = 0; s <= (maxsock); s++) {
     // if(DESCRIPTIVE) printf("\t\tLooking at socket: %d \n",s);

      //SOCKET IS READY FOR WRITING
      if(FD_ISSET(s, &writesocks)){ 
        int peer_key;
        peer_key = get_peer_key_from_sockfd(s);

        if(peer_key < 0){
          if(DESCRIPTIVE) printf("\t\tWas not able to get peer from socket %d\n",s);
          exit(EXIT_FAILURE);
        }

        if(DESCRIPTIVE) printf("\t\tPeer %d is ready for writing and has state: %d\n",peer_key,peer_connections[peer_key].state);

        if(DESCRIPTIVE) printf("\t(%d) %s:%s\n",peer_key,peer_connections[peer_key].hostname,peer_connections[peer_key].port_str);
        struct addrinfo phints; /* Used to define our address structure */
        memset(&phints,0,sizeof(phints)); /* make sure the addr info is empty */
        phints.ai_family = AF_INET; /* We want IPv4 addresses */
        phints.ai_flags = 0; /* not really sure */
        phints.ai_socktype = SOCK_STREAM; /* We want a strea */
        phints.ai_protocol = 0; /* any protocol we can get son */

        int addr_info_status; /* used to hold status of getaddrinfo */
        struct addrinfo *serv_result;
        if(DESCRIPTIVE) printf("\t\t(%d) getting address info\n",peer_key);
        addr_info_status = getaddrinfo(peer_connections[peer_key].hostname, peer_connections[peer_key].port_str, &phints, &serv_result);

        /* check if the our getting of address informaton was sugcessful and exit if not*/
        if(addr_info_status != 0){
          fprintf(stderr, "getaddrinfo error: %s\n", strerror(errno));
        }

        /* CONNECT! */
        if(DESCRIPTIVE) printf("\t\t(%d) trying to connect through socket again\n",peer_key);
        int connect_status;
        connect_status = connect(peer_connections[peer_key].sock_fd, serv_result->ai_addr, serv_result->ai_addrlen);

        freeaddrinfo(serv_result);

        if(connect_status == 0){
          if(DESCRIPTIVE) printf("\t\t(%d) connected!! \n",peer_key);
          if(send_handshake_to_peer(peer_key)<0){
            //failed to send handshake to peer
            if(DESCRIPTIVE) printf("\t\t(%d) Failed to send handshake to peer, marking as STATE_PEER_INVALID \n",peer_key);
            peer_connections[peer_key].state = STATE_PEER_INVALID;
            peer_connections[peer_key].set = 0;
          }else{
            //now set socket into blocking mode:
            fd_set_blocking(peer_connections[peer_key].sock_fd,1);
            peer_connections[peer_key].state = SENT_HANDSHAKE_WAITING_FOR_REPLY;
          }
        }else if(connect_status == -1){
          if(DESCRIPTIVE) printf("\t\t(%d) Could not connect the second time.\n",peer_key);
          peer_connections[peer_key].state = STATE_PEER_INVALID;
          peer_connections[peer_key].set = 0;
        }
      }

      if (FD_ISSET(s, &readsocks)) {
        if (s == server_sock) { // handle a read on our server's socket
          if(DESCRIPTIVE) printf("\t\t(%d) Got a connection on our listening socket\n",s);
            /* New connection */
            int newsock;
            struct sockaddr_in their_addr;
            socklen_t size = sizeof(struct sockaddr_in);
            newsock = accept(server_sock, (struct sockaddr *)&their_addr, &size);
            if (newsock == -1) {
              fprintf(stderr, "Error accepting new connection: %s\n", strerror(errno));
            }else {
              /* ADD THE NEW SOCCET TO OUR CONNECTED PEERS STRUCT */
              /* Handle read */
              socklen_t len;
              struct sockaddr_storage peer_addr;
              char ipstr[INET6_ADDRSTRLEN];
              int port;

              len = sizeof peer_addr;
              getpeername(newsock, (struct sockaddr*)&peer_addr, &len);

              struct sockaddr_in *s_in = (struct sockaddr_in *)&peer_addr;
              port = ntohs(s_in->sin_port);
              inet_ntop(AF_INET, &s_in->sin_addr, ipstr, sizeof ipstr);

              int new_peer_key = max_peer_key + 1;
              max_peer_key = new_peer_key;
              peer_connections[new_peer_key].set = 1;
              peer_connections[new_peer_key].sock_fd = newsock;
              peer_connections[new_peer_key].port = port;
              strcpy(peer_connections[new_peer_key].hostname,ipstr);
              snprintf(peer_connections[new_peer_key].port_str, sizeof peer_connections[new_peer_key].port_str, "%d",port);

              peer_connections[new_peer_key].state = ACCEPTED_WATING_FOR_HANDSHAKE;

            }
          }else{ //WE ARE READY FOR READING ON A NON SERVER SOCK
            /** this is not a new connection lets read from the socket!! **/
            int socket_peer_key;
            socket_peer_key = get_peer_key_from_sockfd(s);

            if(DESCRIPTIVE) printf("\t\t\t\tPeer: %d\n", socket_peer_key);

            int read_results;
            read_results = read_from_peer(socket_peer_key);

            if(read_results == -1){ //if the message is an exit.
              break;
            }            
        }
      }
    }
    //update the nowtime
    gettimeofday(&now_time,NULL);
  }

  // close the file we have been writing to.
  fclose(file_fp);

  if(DESCRIPTIVE) printf("We are done connecting to our peers. Sending stop to tracker...\n");
  send_stop_to_tracker();

  //print our state
  print_peers_state();

  free_peer_connections();


  if(DESCRIPTIVE) printf("\n\n\n\n");
  if(DESCRIPTIVE) Print_Bit_Set(file_bitset,num_pieces_in_torrent);
  exit(EXIT_SUCCESS);
}